﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gear : MonoBehaviour
{
    public bool IsRusted;
    public bool IsRotateActive;
    public bool TurnRigth;

    float generalSpeed = 100f;
    public Renderer renderer;
    MaterialPropertyBlock block;
    int opacityID;
    int colorID;

    Color initialColor;

    private void Awake()
    {
        block = new MaterialPropertyBlock();
        opacityID = Shader.PropertyToID("_Opacity");
        colorID = Shader.PropertyToID("_ColorB");
    }
    private void OnEnable()
    {
        rustValue = 0;
    }
    float rustValue = 0;

    float rotationZ = 0f;
    void Rotate(float speed)
    {
        rotationZ += speed * Time.deltaTime;
        transform.localRotation = Quaternion.Euler(new Vector3(0, 0, rotationZ));
    }
    private void Update()
    {
        if (IsRotateActive)
        {
            float turnSpeed = 0;
            if (!TurnRigth)
            {
                turnSpeed = generalSpeed / transform.localScale.x;
            }

            else
            {
                turnSpeed = -generalSpeed / transform.localScale.x;
            }
            Rotate(turnSpeed);
        }
    }
    public void UpdateRustValue()
    {
        rustValue += 2 * Time.deltaTime;
        if (rustValue >= 1f)
        {
            rustValue = 1f;
            IsRusted = false;
            GameLogic.Instance.IncreaseGearComplete();
        }
        ChangeOpacity(rustValue);
    }
    public void ChangeOpacity(float value)
    {
        block.SetFloat(opacityID, value);
        renderer.SetPropertyBlock(block);
    }
    public void ChangeColor(Color c)
    {
        block.SetColor(colorID, c);
        renderer.SetPropertyBlock(block);
    }
}
