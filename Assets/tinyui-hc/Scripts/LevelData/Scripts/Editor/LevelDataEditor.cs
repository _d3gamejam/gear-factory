﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LevelDataEditor : EditorWindow
{
    string assetName;
    GameObject levelObject;

    [MenuItem("LevelCreator/Create")]
    static void Init()
    {
        LevelDataEditor window = (LevelDataEditor)EditorWindow.GetWindow(typeof(LevelDataEditor));
        window.minSize = new Vector2(400, 200);
        window.maxSize = new Vector2(400, 200);
        window.ShowUtility();
    }

    [System.Obsolete]
    private void OnGUI()
    {

        EditorGUILayout.Space();

        EditorGUILayout.Space();
        ScriptableObject target = this;
        SerializedObject so = new SerializedObject(target);

        assetName = EditorGUILayout.TextField("Asset Name", assetName);
        levelObject = EditorGUILayout.ObjectField("LevelObject", levelObject, typeof(GameObject), true) as GameObject;

        if (GUILayout.Button("Save Level"))
        {
            List<Gear> gameObjects = new List<Gear>();
            foreach (Transform c in levelObject.GetComponentsInChildren(typeof(Transform), true))
            {
                if (c.gameObject != levelObject && c.gameObject.layer == 9)
                    gameObjects.Add(c.GetComponent<Gear>());
            }
            CreateAsset(assetName, gameObjects);
        }
    }

    [System.Obsolete]
    public static void CreateAsset(string name, List<Gear> gameObjects)
    {
        LevelData asset = CreateInstance<LevelData>();

        string path = "Assets/Scripts/LevelData/Data";

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/" + name + ".asset");

        foreach (Gear g in gameObjects)
        {
            ObjectData lData;

            lData.IsRusted = g.IsRusted;
            lData.TurnRight = g.TurnRigth;
            lData.Position = g.transform.localPosition;
            lData.Rotation = g.transform.localRotation;
            lData.Scale = g.transform.localScale;
            //lData.ObjectPrefab = PrefabUtility.GetCorrespondingObjectFromSource(g);
            asset.Data.Add(lData);
        }


        AssetDatabase.CreateAsset(asset, assetPathAndName);

        EditorUtility.SetDirty(asset);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();

    }

    void OnInspectorUpdate()
    {
        Repaint();
    }
}
