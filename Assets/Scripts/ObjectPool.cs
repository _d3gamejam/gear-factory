﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ObjectPool : MonoBehaviour
{
    public static ObjectPool Instance { get; private set; }
    private int poolSize = 20;
    public GameObject gearObj;
    List<Gear> inativePool = new List<Gear>();
    List<Gear> activePool = new List<Gear>();

    public Transform LevelContainer;

    Vector3 initialPosition = Vector3.one * 5000;

    void Awake()
    {
        Instance = this;
        InitPool();
    }

    private void InitPool()
    {
        for (int i = 0; i < poolSize; i++)
        {
            GameObject instance = Instantiate(gearObj, Vector3.zero, Quaternion.identity, transform);
            instance.SetActive(false);
            instance.transform.position = initialPosition;
            inativePool.Add(instance.GetComponent<Gear>());
        }
    }

    public Gear GetGear()
    {
        if (inativePool.Count > 0)
        {
            Gear gear = inativePool[0];
            gear.transform.parent = LevelContainer;
            gear.gameObject.SetActive(true);
            activePool.Add(gear);
            inativePool.RemoveAt(0);
            return gear;
        }
        return null;
    }

    public void Remove(Gear gear)
    {
        if (activePool.Count > 0)
        {
            if (activePool.Contains(gear))
            {
                gear.IsRotateActive = false;
                activePool.Remove(gear);
                gear.transform.parent = transform;
                gear.gameObject.SetActive(false);
                inativePool.Add(gear);
            }
        }

    }

    public void RemoveAll()
    {
        if (activePool.Count > 0)
        {
            int count = activePool.Count;
            for (int i = 0; i < count; i++)
            {

                Remove(activePool[0]);
            }
        }
    }

}
