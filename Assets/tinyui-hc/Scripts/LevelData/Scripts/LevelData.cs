﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="LevelData",menuName ="GameSettings/LevelData")]
public class LevelData : ScriptableObject
{
    public int LevelIndex;
    public List<ObjectData> Data = new List<ObjectData>();  
}
[System.Serializable]
public struct ObjectData
{
    public bool IsRusted;
    public bool TurnRight;
    public Vector3 Position;
    public Quaternion Rotation;
    public Vector3 Scale;
}
