﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    private void Awake()
    {
        Instance = this;

        Physics.reuseCollisionCallbacks = true;
        Application.targetFrameRate = 60;
        QualitySettings.vSyncCount = 0;
        SceneManager.LoadScene(1, LoadSceneMode.Additive);
    }
}
