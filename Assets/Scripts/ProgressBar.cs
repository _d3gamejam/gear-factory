﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ProgressBar : MonoBehaviour
{
    public RectTransform FillTransform;
    public TextMeshProUGUI CurrentLevelText;
    public TextMeshProUGUI NextLevelText;

    private float minValue = 77.8f;
    private float maxValue = 900;

    private float partSize;

    public void SetupBar(int currentLevel)
    {
        FillTransform.sizeDelta = new Vector2(minValue, FillTransform.sizeDelta.y);
        CurrentLevelText.text = currentLevel.ToString();
        NextLevelText.text = (currentLevel + 1).ToString();
    }
    public void SetBar()
    {

    }

    public float CalculateFillPosition(float maxValue, float minValue, float fillRate)
    {
        return (((maxValue - minValue)) * fillRate) + minValue;
    }
}
