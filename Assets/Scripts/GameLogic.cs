﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour
{
    public static GameLogic Instance { get; private set; }

    private int currentRustedCount = 0;
    private void Awake()
    {
        Instance = this;
    }

    public void IncreaseGearComplete()
    {
        currentRustedCount--;
        if (currentRustedCount <= 0)
            LevelManager.Instance.IncreaseLevel();
    }
    public void SetRustedGearCount(int rustedCount)
    {
        currentRustedCount = rustedCount;
    }
}
