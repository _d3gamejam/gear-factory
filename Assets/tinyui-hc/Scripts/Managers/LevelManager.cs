﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }

    public Color[] Colors;
    public static System.Action onLevelStarted;
    public Animator RobotAnimator;
    private static void FireOnLevelStarted()
    {
        if (onLevelStarted != null)
            onLevelStarted();
    }
    public static System.Action onLevelCompleted;
    private static void FireOnLevelCompleted()
    {
        if (onLevelCompleted != null)
            onLevelCompleted();
    }

    [HideInInspector]
    public LevelData CurrentLevelData;
    public List<LevelData> Levels;

    public int CurrentLevelIndex
    {
        get { return PlayerPrefs.GetInt("CurrentLevelIndex", 0); }
        set { PlayerPrefs.SetInt("CurrentLevelIndex", value); }
    }
    public int ImaginaryLevel
    {
        get { return PlayerPrefs.GetInt("ImaginaryLevel", 1); }
        set { PlayerPrefs.SetInt("ImaginaryLevel", value); }
    }

    private void Awake()
    {
        Instance = this;
        CreateLevels();
    }
    private void OnEnable()
    {
       
    }
    private void OnDisable()
    {
        
    }
    private void Start()
    {
        RobotAnimator.Play("LevelStartIdle", 0, 0);
        
    }
    void CreateLevels()
    {

    }

    List<Gear> activeGears = new List<Gear>();
    bool firstLevel = true;
    void StartLevel(int levelIndex)
    {
        robotAnimCompleted = false;
        activeGears.Clear();
        CurrentLevelData = Levels[levelIndex];
        int currentRustedCount = 0;
        foreach (ObjectData od in CurrentLevelData.Data)
        {
            Gear g = ObjectPool.Instance.GetGear();
            g.transform.localPosition = od.Position;
            g.transform.localRotation = od.Rotation;
            g.transform.localScale = od.Scale;
            g.ChangeColor(Colors[Random.Range(0, Colors.Length)]);
            g.IsRusted = od.IsRusted;
            g.TurnRigth = od.TurnRight;
            activeGears.Add(g);
            if (od.IsRusted)
            {
                g.ChangeOpacity(0);
                currentRustedCount++;
            }
            else
            {
                g.ChangeOpacity(1);
            }   
        }
        GameLogic.Instance.SetRustedGearCount(currentRustedCount);
        FireOnLevelStarted();
    }

    public void IncreaseLevel()
    {
        for (int i = 0; i < activeGears.Count; i++)
        {
            activeGears[i].IsRotateActive = true;
        }
        
        ImaginaryLevel++;

        CurrentLevelIndex++;
        if (CurrentLevelIndex >= Levels.Count)
            CurrentLevelIndex = 0;

        StartCoroutine(RobotLevelCompleteRoutine());
        //StartCoroutine(FunctionDelay(3f, () => { StartLevel(CurrentLevelIndex); }));
    }

    WaitForEndOfFrame wfeof = new WaitForEndOfFrame();
    bool robotAnimCompleted = false;
    IEnumerator RobotLevelCompleteRoutine()
    {
        robotAnimCompleted = false;
        RobotAnimator.Play("LevelComplete", 0, 0);
        yield return wfeof;
        yield return new WaitForSeconds(RobotAnimator.GetCurrentAnimatorStateInfo(0).length/6*5);
        ResetValues();
        StartLevel(CurrentLevelIndex);
        yield return new WaitForSeconds(RobotAnimator.GetCurrentAnimatorStateInfo(0).length/6);
        RobotAnimator.Play("Idle", 0, 0);
        TinyUIController.Instance.OpenStartScreen();
        robotAnimCompleted = true;
    }
    IEnumerator RobotLevelStartRoutine()
    {
        robotAnimCompleted = false;
        RobotAnimator.Play("LevelStart", 0, 0);
        yield return wfeof;
        yield return new WaitForSeconds(RobotAnimator.GetCurrentAnimatorStateInfo(0).length / 4 * 3);
        StartLevel(CurrentLevelIndex);
        yield return new WaitForSeconds(RobotAnimator.GetCurrentAnimatorStateInfo(0).length / 4);
        RobotAnimator.Play("Idle", 0, 0);
        TinyUIController.Instance.OpenStartScreen();
        robotAnimCompleted = true;
    }
    IEnumerator FunctionDelay(float delay, System.Action func = null)
    {
        yield return new WaitForSeconds(delay);
        yield return new WaitUntil(() => robotAnimCompleted);
        if (func != null)
            func();
    }

    public void RestartLevel()
    {
        ResetValues();
        StartLevel(CurrentLevelIndex);
    }

    void ResetValues()
    {
        ObjectPool.Instance.RemoveAll();
        FireOnLevelCompleted();

        CurrentLevelData = Levels[CurrentLevelIndex];
    }
    public void StartFirst()
    {
        if (firstLevel)
        {
            firstLevel = false;
            StartCoroutine(RobotLevelStartRoutine());
        }
    }
}
