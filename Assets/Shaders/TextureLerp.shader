// Upgrade NOTE: upgraded instancing buffer 'TextureLerp' to new syntax.

// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "TextureLerp"
{
	Properties
	{
		_TextureA("TextureA", 2D) = "white" {}
		_ColorA("ColorA", Color) = (0,0,0,0)
		_TextureB("TextureB", 2D) = "white" {}
		_ColorB("ColorB", Color) = (0,0,0,0)
		_Opacity("Opacity", Range( 0 , 1)) = 0
		_OpacityMask("OpacityMask", 2D) = "white" {}
		[Toggle]_EmissionSwitch("EmissionSwitch", Float) = 0
		_Emission("Emission", Color) = (0,0,0,0)
		_Metallic("Metallic", Range( 0 , 1)) = 0
		_Smoothness("Smoothness", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma multi_compile_instancing
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _TextureA;
		uniform sampler2D _TextureB;
		uniform sampler2D _OpacityMask;
		uniform float _EmissionSwitch;
		uniform float4 _Emission;
		uniform float _Metallic;
		uniform float _Smoothness;

		UNITY_INSTANCING_BUFFER_START(TextureLerp)
			UNITY_DEFINE_INSTANCED_PROP(float4, _ColorA)
#define _ColorA_arr TextureLerp
			UNITY_DEFINE_INSTANCED_PROP(float4, _TextureA_ST)
#define _TextureA_ST_arr TextureLerp
			UNITY_DEFINE_INSTANCED_PROP(float4, _ColorB)
#define _ColorB_arr TextureLerp
			UNITY_DEFINE_INSTANCED_PROP(float4, _TextureB_ST)
#define _TextureB_ST_arr TextureLerp
			UNITY_DEFINE_INSTANCED_PROP(float4, _OpacityMask_ST)
#define _OpacityMask_ST_arr TextureLerp
			UNITY_DEFINE_INSTANCED_PROP(float, _Opacity)
#define _Opacity_arr TextureLerp
		UNITY_INSTANCING_BUFFER_END(TextureLerp)

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float4 _ColorA_Instance = UNITY_ACCESS_INSTANCED_PROP(_ColorA_arr, _ColorA);
			float4 _TextureA_ST_Instance = UNITY_ACCESS_INSTANCED_PROP(_TextureA_ST_arr, _TextureA_ST);
			float2 uv_TextureA = i.uv_texcoord * _TextureA_ST_Instance.xy + _TextureA_ST_Instance.zw;
			float4 _ColorB_Instance = UNITY_ACCESS_INSTANCED_PROP(_ColorB_arr, _ColorB);
			float4 _TextureB_ST_Instance = UNITY_ACCESS_INSTANCED_PROP(_TextureB_ST_arr, _TextureB_ST);
			float2 uv_TextureB = i.uv_texcoord * _TextureB_ST_Instance.xy + _TextureB_ST_Instance.zw;
			float4 _OpacityMask_ST_Instance = UNITY_ACCESS_INSTANCED_PROP(_OpacityMask_ST_arr, _OpacityMask_ST);
			float2 uv_OpacityMask = i.uv_texcoord * _OpacityMask_ST_Instance.xy + _OpacityMask_ST_Instance.zw;
			float _Opacity_Instance = UNITY_ACCESS_INSTANCED_PROP(_Opacity_arr, _Opacity);
			float4 temp_cast_0 = (_Opacity_Instance).xxxx;
			float4 lerpResult3 = lerp( ( _ColorA_Instance * tex2D( _TextureA, uv_TextureA ) ) , ( _ColorB_Instance * tex2D( _TextureB, uv_TextureB ) ) , max( round( tex2D( _OpacityMask, uv_OpacityMask ) ) , temp_cast_0 ));
			o.Albedo = lerpResult3.rgb;
			o.Emission = (( _EmissionSwitch )?( _Emission ):( float4( 0,0,0,0 ) )).rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Smoothness;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=17700
0;531;948;469;924.4769;131.4211;1.841723;True;True
Node;AmplifyShaderEditor.SamplerNode;13;-1509.306,482.4268;Inherit;True;Property;_OpacityMask;OpacityMask;5;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;1;-1661.719,-538.4025;Inherit;True;Property;_TextureA;TextureA;0;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;5;-1497.426,689.0026;Inherit;False;InstancedProperty;_Opacity;Opacity;4;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RoundOpNode;18;-1153.516,494.8581;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;2;-1638.717,-67.68346;Inherit;True;Property;_TextureB;TextureB;2;0;Create;True;0;0;False;0;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;30;-1284.355,-651.4955;Inherit;False;InstancedProperty;_ColorA;ColorA;1;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;33;-1268.877,-187.1369;Inherit;False;InstancedProperty;_ColorB;ColorB;3;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMaxOpNode;14;-981.3212,584.3498;Inherit;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;32;-1004.778,23.37019;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;-968.8369,-451.4003;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;34;-631.384,-33.45885;Inherit;False;Property;_Emission;Emission;7;0;Create;True;0;0;False;0;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;3;-648.9988,-233.7149;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ToggleSwitchNode;35;-366.1755,7.058949;Inherit;False;Property;_EmissionSwitch;EmissionSwitch;6;0;Create;True;0;0;False;0;0;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;36;-357.2262,146.6792;Inherit;False;Property;_Metallic;Metallic;8;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;37;-355.3846,244.2904;Inherit;False;Property;_Smoothness;Smoothness;9;0;Create;True;0;0;False;0;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;TextureLerp;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;18;0;13;0
WireConnection;14;0;18;0
WireConnection;14;1;5;0
WireConnection;32;0;33;0
WireConnection;32;1;2;0
WireConnection;31;0;30;0
WireConnection;31;1;1;0
WireConnection;3;0;31;0
WireConnection;3;1;32;0
WireConnection;3;2;14;0
WireConnection;35;1;34;0
WireConnection;0;0;3;0
WireConnection;0;2;35;0
WireConnection;0;3;36;0
WireConnection;0;4;37;0
ASEEND*/
//CHKSM=F33933BD7705E0E43F78F423DD7CFD833C3BDFDE