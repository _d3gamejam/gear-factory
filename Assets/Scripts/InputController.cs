﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public Camera InputCamera;
    Rigidbody body;
    public GameObject OilParticle;
    public Animator OilAnimator;

    Vector3 startPos;

    private void Awake()
    {
        body = GetComponent<Rigidbody>();
        startPos = transform.localPosition;
    }
    private void Update()
    {
        Move();
    }
    private void OnEnable()
    {
        oilTimer = 1f;
        isPlaying = false;
        LevelManager.onLevelStarted += Handle_LevelStart;
    }
    private void OnDisable()
    {
        LevelManager.onLevelStarted -= Handle_LevelStart;
    }

    bool firstClick = false;
    private void Handle_LevelStart()
    {
        transform.localPosition = startPos;
        firstClick = false;
    }

    bool centerTaken = false;
    Vector2 initialMousePos = new Vector2(9999, 9999);
    Vector3 lastCharacterPos;
    void Move()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 mousePos = Input.mousePosition;
            Vector2 realPos = InputCamera.ScreenToWorldPoint(mousePos);

            if (!centerTaken)
            {
                initialMousePos = realPos;
                Vector3 tempVector = (Vector3)initialMousePos + (Vector3.up * 1.5f);
                tempVector.z = body.position.z;
                body.position = tempVector;
                lastCharacterPos = body.position;

                centerTaken = true;

                if (!firstClick)
                {
                    firstClick = true;
                    TinyUIController.Instance.OpenGameScreen();
                    LevelManager.Instance.StartFirst();
                }   
            }

            Vector3 direction = (Vector3)realPos - (Vector3)initialMousePos;

            if (direction.magnitude != 0)
            {
                body.position = Vector3.Lerp(body.position, lastCharacterPos + direction, 10f * Time.deltaTime);
                if (Vector3.Distance(body.position, lastCharacterPos + direction) > 0.15f && gear != null && gear.IsRusted)
                {
                    gear.UpdateRustValue();
                    if (!isPlaying)
                        StartCoroutine(OilParticleRoutine());
                }
                    
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            centerTaken = false;
        }
    }

    Gear gear = null;
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent.gameObject.layer == 9)
        {
            gear = other.transform.parent.GetComponent<Gear>();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.transform.parent.gameObject.layer == 9)
        {
            gear = null;
        }
    }

    WaitForEndOfFrame wfeof = new WaitForEndOfFrame();
    float oilTimer = 1f;
    bool isPlaying = false;
    IEnumerator OilParticleRoutine()
    {
        isPlaying = true;
        OilParticle.SetActive(true);
        OilAnimator.Play("OilSpilling", 0, 0);
        while (true)
        {
            oilTimer -= Time.deltaTime;
            if (oilTimer <= 0)
                break;
            yield return wfeof;
        }
        isPlaying = false;
        oilTimer = 1f;
        OilAnimator.Play("OilIdle", 0, 0);
        OilParticle.SetActive(false);
    }
}
